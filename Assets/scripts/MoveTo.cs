using System.Collections;
using System.Collections.Generic;
using System.Resources;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class MoveTo : MonoBehaviour {
	public Animator animator;
	public NavMeshAgent agent;
	public Text sleepText, foodText, waterText, restText;
	public Slider sleepSlider,foodSlider,waterSlider,restSlider;
	public int target = -1;
	public List<Transform> points;
	public float sleep, food, water, rest;
	public int minSleep, minFood, minWater, minRest;
	public float timeToSleep, timeToFood, timeToWater, timeToRest;
	float time;
	public float TimerMax;

	void Awake() {
		Application.targetFrameRate = 60;
	}
	void Update() {
		time += Time.deltaTime;
		if (time >= TimerMax) {
			time = 0;
			sleep = Mathf.Clamp(sleep -= 100 / timeToSleep * TimerMax, 0f, 100f);
			food = Mathf.Clamp(food -= 100 / timeToFood * TimerMax, 0f, 100f);
			water = Mathf.Clamp(water -= 100 / timeToWater * TimerMax, 0f, 100f);
			rest = Mathf.Clamp(rest -= 100 / timeToRest * TimerMax, 0f, 100f);
			sleepText.text = "sleep " + Mathf.RoundToInt(sleep).ToString();
			sleepSlider.value = sleep;
			foodText.text = "food " + Mathf.RoundToInt(food).ToString();
			foodSlider.value = food;
			waterText.text = "water " + Mathf.RoundToInt(water).ToString();
			waterSlider.value = water;
			restText.text = "rest " + Mathf.RoundToInt(rest).ToString();
			restSlider.value = rest;
			if (target == -1) {
				if (sleep <= minSleep) {
					agent.SetDestination(points[0].transform.position);
					StartCoroutine(WaitForAction(0));
					target = 0;
				}
				else if (food <= minFood) {
					agent.SetDestination(points[1].transform.position);
					StartCoroutine(WaitForAction(1));
					target = 1;
				}
				else if (water <= minWater) {
					agent.SetDestination(points[2].transform.position);
					StartCoroutine(WaitForAction(2));
					target = 2;
				}
				else if (rest <= minRest) {
					agent.SetDestination(points[3].transform.position);
					StartCoroutine(WaitForAction(3));
					target = 3;
				}
				else {
					agent.SetDestination(points[4].transform.position);
				}
			}
		}
		animator.SetBool("running", (agent.velocity.magnitude >= 1 ? true : false));
	}
	IEnumerator WaitForAction(int ID) {
		yield return new WaitForSeconds(1f);
		print(ID);
		while (agent.velocity.magnitude >= 1 && Vector3.Distance(transform.position, points[ID].position) > 5) { print("while"); yield return new WaitForSeconds(.2f); }
		print(Vector3.Distance(transform.position, points[ID].position));
		switch (ID) {
			case 0:
                //animacja on
                RotateTo(points[6].transform.position);
                yield return new WaitForSeconds(1f);
                animator.SetBool("sleeping", true);
				yield return new WaitForSeconds(10);
				animator.SetBool("sleeping", false);
                yield return new WaitForSeconds(6.5f);
                //animacja off
                sleep = 100;
				sleepText.text = "sleep " + Mathf.RoundToInt(sleep).ToString();
				break;
			case 1:
				//animacja on
				animator.SetBool("eating", true);
				yield return new WaitForSeconds(5);
				animator.SetBool("eating", false);
				//animacja off
				food = 100;
				foodText.text = "food " + Mathf.RoundToInt(food).ToString();
				break;
			case 2:
				//animacja on
				animator.SetBool("drinking", true);
				yield return new WaitForSeconds(5);
				animator.SetBool("drinking", false);
				//animacja off
				water = 100;
				waterText.text = "water " + Mathf.RoundToInt(water).ToString();
				break;
			case 3:
                //animacja on
				RotateTo(points[5].transform.position);
				yield return new WaitForSeconds(1f);
                animator.SetBool("resting", true);
				yield return new WaitForSeconds(15);
				animator.SetBool("resting", false);
                yield return new WaitForSeconds(2f);
                //animacja off
                rest = 100;
				restText.text = "rest " + Mathf.RoundToInt(rest).ToString();
				break;
		}
		target = -1;
    }
	void RotateTo(Vector3 to) {
        transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, new Vector3(to.x - transform.position.x, 0, to.z - transform.position.z), 90, .0f));
    }
}